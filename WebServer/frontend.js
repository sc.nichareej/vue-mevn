const express = require('express');
const path = require('path');
const app = express();

let port = 8999;

app.use(express.static(path.join(__dirname, '../dist')));
app.get('*', function (req, res) {
  console.log('res.sendFile', path.join(__dirname + '../dist/index.html'))
    res.sendFile(path.join(__dirname + '../dist/index.html'));
});

const http = require('http');
http.createServer(app).listen(port, '0.0.0.0', function (err) {
  console.log(`Ready on http://0.0.0.0:${port}`);
});