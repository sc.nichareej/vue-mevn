const development = {
  api_url : 'http://127.0.0.1:3000',
  
};


const production = {
  api_url : 'http://10.1.2.141:2555',
  
};


const config = () => {
  console.log('process.env.NODE_ENV', process.env.NODE_ENV)
  if(process.env.NODE_ENV === 'production'){
    return production
  }else if(process.env.NODE_ENV === 'development'){
    return development
  }else{
    return development
  }
}


export default {
    config
  }
