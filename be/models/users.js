module.exports = mongoose => {
    const schema = mongoose.Schema(
        {
            name: {
                type: String,
                default: null
            },
            email: {
                type: String,
                default: null
            },
            phone: {
                type: String,
                default: null
            },
            sex: {
                type: String,
                default: null
            },
            birthdate: {
                type: Date,
                default: null
            },
            address: {
                type: String,
                default: null
            },
            company: {
                type: String,
                default: null
            },
            createdAt: {
                type: Number,
                default: null
            },
            updatedAt: {
                type: Number,
                default: null
            }
        },
        { timestamps: true }
    );

    schema.method("toJSON", function () {
        const { __v, _id, ...object } = this.toObject();
        object.id = _id;
        return object;
    });

    return mongoose.model("users", schema);
};
