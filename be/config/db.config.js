var DB_URL = process.env.DB_URL || "localhost:27017";
var DB_NAME = process.env.DB_NAME || "vue-mevn";
module.exports = {
  url: "mongodb://" + DB_URL + "/" + DB_NAME
};