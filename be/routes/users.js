var express = require('express');
var router = express.Router();
const db = require("../models");
const UsersModel = db.users;
var ObjectId = require('mongodb').ObjectId;

/* GET users */
router.get('/search', function (req, res) {

    var condition = req.query
    console.log('condition', condition)
    // return
    if (condition.name) condition.name = { $regex: '.*' + condition.name + '.*' }

    // order by
    var orderbyObj = {};
    var orderbySplit = req.query.orderby ? req.query.orderby.split('|') : '';
    var orderby = orderbySplit.length === 2 ? { [orderbySplit[0]]: orderbySplit[1].toLowerCase() } : {};

    // limit
    var limit = +req.query.limit || 0;
    var offset = +req.query.offset || 0;
    // remove empty
    for (const key in condition) {
        if (Object.hasOwnProperty.call(condition, key)) {
            if (!condition[key]) {
                delete condition[key]
            }
        }
    }
    console.log('LOG ~ condition', condition)
    console.log('LOG ~ limit', limit)
    console.log('LOG ~ offset', offset)
    console.log('LOG ~ orderby', orderby)
    UsersModel.find(condition)
        .limit(limit)
        .skip(offset)
        .sort(orderby)
        .then(data => {
            if (data.length > 0) {
                res.send({
                    resultCode: "20000",
                    resultDescription: "Success",
                    resultData: data,
                    rowCount: data.length
                });

            }
            else {
                throw 404
            }

        })
        .catch(error => {
            if (error == 404) {
                res.status(404).send({
                    resultCode: "40400",
                    resultDescription: "Data not found",
                });

            }
            else {
                res.status(500).send({
                    resultCode: "50000",
                    resultDescription: "System Error",
                    resultData: error.message || "System Error"
                });
            }
        });
});

/* GET */
router.get('/', async function (req, res) {
    try {
        throw 40300
    } catch (error) {
        console.log('LOG ~ error', error)
        if (error == 40300) {
            res.status(403).send({
                resultCode: "40300",
                resultDescription: "Missing or Invalid parameter",
            });
        }
        else if (error == 404) {
            res.status(404).send({
                resultCode: "40400",
                resultDescription: "Data not found",
            });

        }
        else {
            res.status(500).send({
                resultCode: "50000",
                resultDescription: "System Error",
                resultData: error.message || "System Error"
            });
        }

    }
});

/* GET user by id */
router.get('/:id', async function (req, res) {
    try {
        // let checkId = ObjectId.isValid(req.params.id);
        // if (checkId) {
        const id = req.params.id;
        console.log('id', id)
        let resultFindUsersModel = await UsersModel.findOne({ _id: id })
        console.log('resultFindUsersModel', resultFindUsersModel)
        if (id && resultFindUsersModel) {
            res.send({
                resultCode: "20000",
                resultDescription: "Success",
                resultData: resultFindUsersModel

            });
        } else {
            throw 404
        }
        // }
        // else {
        //     throw 40300
        // }
    } catch (error) {
        console.log('LOG ~ error', error)
        if (error.code == 11000) {
            res.status(403).send({
                resultCode: "40301",
                resultDescription: "Data Duplicate",
            });
        }
        else if (error == 40300) {
            res.status(403).send({
                resultCode: "40300",
                resultDescription: "Missing or Invalid parameter",
            });
        }
        else if (error == 404) {
            res.status(404).send({
                resultCode: "40400",
                resultDescription: "Data not found",
            });

        }
        else {
            res.status(500).send({
                resultCode: "50000",
                resultDescription: "System Error",
                resultData: error.message || "System Error"
            });
        }

    }
});

/* DELETE */
// router.delete('/delete/', async function (req, res) {
//     try {
//         throw 40300
//     } catch (error) {
//         console.log('LOG ~ error', error)
//         if (error == 40300) {
//             res.status(403).send({
//                 resultCode: "40300",
//                 resultDescription: "Missing or Invalid parameter",
//             });
//         }
//         else if (error == 404) {
//             res.status(404).send({
//                 resultCode: "40400",
//                 resultDescription: "Data not found",
//             });

//         }
//         else {
//             res.status(500).send({
//                 resultCode: "50000",
//                 resultDescription: "System Error",
//                 resultData: error.message || "System Error"
//             });
//         }

//     }
// });

/* DELETE user by id */
router.delete('/delete/:id', async function (req, res) {
    try {
        // let checkId = ObjectId.isValid(req.params.id);
        // if (checkId) {
            const _id = req.params.id;
            let resultFindUsersModel = await UsersModel.findByIdAndDelete(req.params.id)
            res.send({
                resultCode: "20000",
                resultDescription: "Success"
            });
        // }
    } catch (error) {
        console.log('LOG ~ error', error)
        if (error.code == 11000) {
            res.status(403).send({
                resultCode: "40301",
                resultDescription: "Data Duplicate",
            });
        }
        else if (error == 40300) {
            res.status(403).send({
                resultCode: "40300",
                resultDescription: "Missing or Invalid parameter",
            });
        }
        else if (error == 404) {
            res.status(404).send({
                resultCode: "40400",
                resultDescription: "Data not found",
            });

        }
        else {
            res.status(500).send({
                resultCode: "50000",
                resultDescription: "System Error",
                resultData: error.message || "System Error"
            });
        }
    }
});

/* POST users */
router.post('/create', async function (req, res) {
    try {
        // Create user in the database
        const resultCreateUserInfo = await UsersModel.create({
            name: req.body.name,
            email: req.body.email,
            phone: req.body.phone,
            birthdate: req.body.birthdate,
            sex: req.body.sex,
            address: req.body.address,
            company: req.body.company,
            createdAt: Date.now(),
            updatedAt: Date.now()
        }
        );
        console.log('resultCreateUserInfo', resultCreateUserInfo)

        res.send({
            resultCode: "20000",
            resultDescription: "Success",
            resultData: {
                id: resultCreateUserInfo._id
            }

        });

    } catch (error) {
        console.log('LOG ~ error', error)
        console.log('LOG ~ error.code', error.code)
        if (error.code == 11000) {
            res.status(403).send({
                resultCode: "40301",
                resultDescription: "Data Duplicate",
            });
        }
        else if (error == 40300) {
            res.status(403).send({
                resultCode: "40300",
                resultDescription: "Missing or Invalid parameter",
            });
        }
        else if (error == 404) {
            res.status(404).send({
                resultCode: "40400",
                resultDescription: "Data not found",
            });

        }
        else {
            res.status(500).send({
                resultCode: "50000",
                resultDescription: "System Error",
                resultData: error.message || "System Error"
            });
        }
    }
});

/* PUT user by id */
router.put('/update/', async function (req, res) {
    try {
        throw 40300
    } catch (error) {
        console.log('LOG ~ error', error)
        if (error == 40300) {
            res.status(403).send({
                resultCode: "40300",
                resultDescription: "Missing or Invalid parameter",
            });
        }
        else if (error == 404) {
            res.status(404).send({
                resultCode: "40400",
                resultDescription: "Data not found",
            });

        }
        else {
            res.status(500).send({
                resultCode: "50000",
                resultDescription: "System Error",
                resultData: error.message || "System Error"
            });
        }

    }
});

/* PUT users */
router.put('/update/:id', async function (req, res) {
    try {
        let resultFindUsersModel = await UsersModel.find({ _id: req.params.id })
        console.log('resultFindUsersModel', resultFindUsersModel)
        console.log(' req.body', req.body)
        if (resultFindUsersModel.length > 0) {
                let data = {}
                if (req.body.name) data.name = req.body.name;
                if (req.body.email) data.email = req.body.email;
                if (req.body.phone) data.phone = req.body.phone;
                if (req.body.birthdate) data.birthdate= req.body.birthdate;
                if (req.body.sex) data.sex= req.body.sex;
                if (req.body.address) data.address= req.body.address;
                if (req.body.company) data.company= req.body.company;
                
                console.log('data', data)

                const resultUpdateUserInfo = await UsersModel.updateOne({ _id: req.params.id }, data);
                console.log('resultUpdateUserInfo : ', resultUpdateUserInfo)
                res.send({
                    resultCode: "20000",
                    resultDescription: "Success",
                });
        }
        else {
            throw 404
        }
      
    } catch (error) {
        console.log('LOG ~ error', error)
        if (error.code == 11000) {
            res.status(403).send({
                resultCode: "40301",
                resultDescription: "Data Duplicate",
            });
        }
        else if (error == 40300) {
            res.status(404).send({
                resultCode: "40300",
                resultDescription: "Missing or Invalid parameter",
            });

        }
        else if (error == 404) {
            res.status(404).send({
                resultCode: "40400",
                resultDescription: "Data not found",
            });

        }
        else {
            res.status(500).send({
                resultCode: "50000",
                resultDescription: "System Error",
                resultData: error.message || "System Error"
            });
        }

    }
});

module.exports = router;
