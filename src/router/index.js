import Vue from "vue";
import VueRouter from "vue-router";
import ListComponent from "@/components/ListComponent.vue";
import ViewComponent from "@/components/ViewComponent.vue";
import CreateComponent from "@/components/CreateComponent.vue";
import EditComponent from "@/components/EditComponent.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "list",
    component: ListComponent,
  },
  {
    path: "/view/:id",
    name: "view",
    component: ViewComponent,
  },
  {
    path: "/create",
    name: "create",
    component: CreateComponent,
  },
  {
    path: "/edit/:id",
    name: "edit",
    component: EditComponent,
  }
  
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
