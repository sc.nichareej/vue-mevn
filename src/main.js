import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import Antd from 'ant-design-vue';
import 'ant-design-vue/dist/antd.css';
import { FormModel } from 'ant-design-vue';
import moment from 'moment-timezone';
import VueFilterDateFormat from 'vue-filter-date-format';

Vue.config.productionTip = false;

Vue.use(Antd);
Vue.use(FormModel);
Vue.use(moment);
Vue.use(VueFilterDateFormat);
Vue.use(require('vue-moment'));

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount("#app");
